﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecHexEditor_2.Project.Source.Models;
using SpecHexEditor_2.Project.Source.Views.ViewHelpers;
using System;

namespace SpecHexEditor_2.UnitTests.ModelsTests
{
    [TestClass]
    public class BinaryFileTests
    {
        [TestMethod]
        public void DoesFillBinaryFileWithContentInHex()
        {
            binaryFile.ParseBinaryContent( binaryFileTestContent );
            string actual = binaryFile.PrintWholeFileAsHex( binaryFileTestContent.Length, binaryFileTestContent.Length );
            string expected = ConvertTestContentToHex();

            Assert.AreEqual( expected, actual );
        }


        [TestMethod]
        public void DoesPrintSingleByteInHex()
        {
            binaryFile.ParseBinaryContent( binaryFileTestContent );
            int byteIndexToTest = 2;
            string expected = ConvertTestContentToHex( Convert.ToByte(binaryFileTestContent[byteIndexToTest] ) );
            string actual = binaryFile.PrintSingleByteAsHex( byteIndexToTest );

            Assert.AreEqual( expected, actual );
        }


        [TestMethod]
        public void DoesPrintNextByteAsHex()
        {
            const int startingIndex = 3;
            const int expectedIndex = startingIndex + 1;

            binaryFile.ParseBinaryContent( binaryFileTestContent );

            binaryFile.PrintSingleByteAsHex( startingIndex );
            string actual = binaryFile.PrintNextByteAsHex();
            string expected = ConvertTestContentToHex( Convert.ToByte( binaryFileTestContent[expectedIndex] ) );

            Assert.AreEqual( expected, actual );
        }


        [TestMethod]
        public void DoesPrintRangeAsHex()
        {
            var range = (start: 3, end: 7);
            binaryFile.ParseBinaryContent( binaryFileTestContent );

            byte[] expectedBytes = new byte[range.end-range.start];
            for( int i = range.start; i < range.end; ++i )
            {
                expectedBytes[i-range.start] = Convert.ToByte( binaryFileTestContent[i] );
            }

            string expected = Formatter.ByteArrayToHEXString( expectedBytes, expectedBytes.Length, expectedBytes.Length );
            string actual = binaryFile.PrintRangeAsHex( range.start, range.end - range.start );

            Assert.AreEqual( expected, actual );
        }


        [TestMethod]
        public void DoesPrintSingleByteInASCII()
        {
            binaryFile.ParseBinaryContent( binaryFileTestContent );
            int byteIndexToTest = 2;
            string expected = binaryFileTestContent[byteIndexToTest].ToString();
            string actual = binaryFile.PrintSingleByteAsASCII( byteIndexToTest );

            Assert.AreEqual( expected, actual, $"{expected} != {actual}" );
        }


        [TestMethod]
        public void DoesPrintNextByteAsASCII()
        {
            const int startingIndex = 3;
            const int expectedIndex = startingIndex + 1;

            binaryFile.ParseBinaryContent( binaryFileTestContent );

            binaryFile.PrintSingleByteAsASCII( startingIndex );
            string actual = binaryFile.PrintNextByteAsASCII();
            string expected = binaryFileTestContent[expectedIndex].ToString();

            Assert.AreEqual( expected, actual );
        }


        [TestMethod]
        public void DoesPrintRangeAsASCII()
        {
            var range = (start: 3, end: 7);
            binaryFile.ParseBinaryContent( binaryFileTestContent );

            string expected = "";
            for( int i = range.start; i < range.end; ++i )
            {
                expected += binaryFileTestContent[i].ToString();
            }

            string actual = binaryFile.PrintRangeAsASCII( range.start, range.end );

            Assert.AreEqual( expected, actual );
        }



        private string ConvertTestContentToHex()
        {
            byte[] bytesAsString = new byte[binaryFileTestContent.Length];
            int byteIndex = 0;
            foreach(char nextByte in binaryFileTestContent)
            {
                bytesAsString[byteIndex++] = Convert.ToByte( nextByte );
            }
            return Formatter.ByteArrayToHEXString( bytesAsString, binaryFileTestContent.Length, binaryFileTestContent.Length );
        }

        private string ConvertTestContentToHex( byte byteToConvert )
        {
            byte[] byteToconvert = new byte[1];
            byteToconvert[0] = byteToConvert;
            return Formatter.ByteArrayToHEXString( byteToconvert, byteToconvert.Length, byteToconvert.Length );
        }


        private string binaryFileTestContent = "abcdefghijklmnoprstuwxyz";
        private BinaryFileLogic binaryFile = new BinaryFileLogic();
    }
}
