﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecHexEditor_2.Project.Source.Views.ViewHelpers
{
    public class Formatter
    {
        /// <summary>
        /// Converts the array of bytes to their hexadecimal represenation as string
        /// </summary>
        static public string ByteArrayToHEXString( byte[] rawBytes, int bytesInColumn, int bytesInRow = 16 )
        {
            bytesInColumn = bytesInColumn > bytesInRow ? bytesInRow : bytesInColumn;

            string bytesAsHex = "";

            for( int rowIndex = 0; rowIndex <= rawBytes.Length / bytesInRow; ++rowIndex )
            {
                for( int columnIndex = 0; columnIndex < bytesInRow / bytesInColumn; ++columnIndex )
                {
                    int conversionStartingPoint = columnIndex * bytesInColumn + bytesInRow * rowIndex;

                    int conversionLength = bytesInRow;
                    if( rawBytes.Length - conversionStartingPoint < bytesInRow )
                    {
                        conversionLength = rawBytes.Length - conversionStartingPoint;
                    }

                    if( conversionStartingPoint < rawBytes.Length )
                    {
                        bytesAsHex += BitConverter.ToString( rawBytes, conversionStartingPoint, conversionLength ).Replace( "-", "" );
                    }
                    bytesAsHex += " ";
                }
                bytesAsHex += "\n";
            }

            return bytesAsHex;
        }


        static public string StringAsBytesToHEXString(string bytes, int bytesInColumn, int bytesInRow = 16)
        {
            return "Not implemented!";
        }


        static private byte[] GetRangeOf( byte[] array, int start, int length )
        {
            byte[] range = new byte[length];

            for( int i = 0; i < length; ++i )
            {
                range[i] = array[i + start];
            }

            return range;
        }
    }
}
