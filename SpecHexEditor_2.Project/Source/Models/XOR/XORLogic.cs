﻿using SpecHexEditor_2.Project.Source.Views.ViewHelpers;

namespace SpecHexEditor_2.Project.Source.Models
{
    public class XORLogic : XORModel
    {
        /// <summary>
        /// Constructor with parameters from Key for XOR and data for XOR fields.
        /// </summary>
        public XORLogic( byte[] key, byte[] data ) : base(key, data)
        {
        }


        /// <summary>
        /// Calculates the XOR. Returns nothing but saves the result to the model.
        /// </summary>
        public void CalculateXOR()
        {
            int dataByteIndex;
            int keyByteIndex = 0;
            for( dataByteIndex = 0; dataByteIndex + keyByteIndex < Data.Length; dataByteIndex += Key.Length )
            {
                for( keyByteIndex = 0; keyByteIndex < Key.Length; ++keyByteIndex )
                {
                    Result.Add( (byte)(Key[keyByteIndex] ^ Data[dataByteIndex + keyByteIndex]) );
                }
            }

            if( Data.Length % dataByteIndex != 0 )
            {
                for( keyByteIndex = 0; keyByteIndex < Data.Length % dataByteIndex; ++keyByteIndex )
                {
                    Result.Add( (byte)(Key[keyByteIndex] ^ Data[dataByteIndex + keyByteIndex]) );
                }
            }
        }



        /// <summary>
        /// Rturns the Result kept in model.
        /// </summary>
        public string PrintResult()
        {
            return Formatter.ByteArrayToHEXString( Result.ToArray(), Result.Count, Result.Count );
        }


        /// <summary>
        /// Rturns the Key kept in model.
        /// </summary>
        public string PrintKey()
        {
            return Formatter.ByteArrayToHEXString( Key, Key.Length, Key.Length );
        }


        /// <summary>
        /// Rturns the Data for XOR kept in model.
        /// </summary>
        public string PrintData()
        {
            return Formatter.ByteArrayToHEXString( Data, Key.Length, Key.Length );
        }
    }
}
