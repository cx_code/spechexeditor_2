﻿using System;
using System.Collections.Generic;


namespace SpecHexEditor_2.Project.Source.Models
{
    public abstract class BinaryFileModel
    {
        /// <summary>
        /// Basic constructor, initializes empty List
        /// </summary>
        public BinaryFileModel()
        {
            binaryFileContent = new List<byte>();
        }



        /// <summary>
        /// Path to the file which should be opened and read
        /// </summary>
        public string binaryFilePath
        { get; set; }

        /// <summary>
        /// The content of the binary file in bytes
        /// </summary>
        protected List<byte> binaryFileContent
        { get; set; }

        /// <summary>
        /// position of the iterator in file
        /// </summary>
        protected int iteratorPosition
        { get; set; }
    }
}
