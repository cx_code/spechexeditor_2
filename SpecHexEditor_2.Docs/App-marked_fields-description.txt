1. Open file dialog
	"Open from file..."

2. Saving to file also as  Open File dialog
	"Save to file..."

3. Basic buttons (all operates on 12 using  Binding):
	"Swap selected bytes"
	"Swap bytes in whole file"
	"Set selected as XOR key"
	"Set selected as data for XOR"

4. Text box
	"XOR key:" - Binding to XOR.Key

5. Text box:
	"Data for XOR:"

6. Basic button with event for XOR operation
	"XOR"

7. Text box with copy operation enabled
	"XOR result:"

8. Basic button with copy operation event
	"Copy result to clipboard"

9. Search operation
	"Search"  - button with search event
	Text box with Binding to Search.Phrase

10. Sorting operation
	"Bytes per:" label/tex block

	"Column:" - Binding to Sort.Column
	"Row:"    - Binding to Sort.Row

11. Text box with Binding to BinaryFile
	Should be prevented from modifying!

12. Text box with Binding to BinaryFile.Content
	Also prevented from modifying!