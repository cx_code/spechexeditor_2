﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace SpecHexEditor_2.Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenFromFileButton_Click( object sender, RoutedEventArgs e )
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            Nullable<bool> result = openFileDialog.ShowDialog();

            if( result == true )
            {
                filePathToOpen = openFileDialog.FileName;
            }
        }

        private void SaveToFileButton_Click( object sender, RoutedEventArgs e )
        {
            SaveFileDialog SaveFileDialog1 = new SaveFileDialog();
            if( SaveFileDialog1.ShowDialog() == true )
            {
                File.WriteAllText( SaveFileDialog1.FileName, HexTextBox.Text );
            }
        }

        private void SwapSelectedBytesButton_Click( object sender, RoutedEventArgs e )
        {

        }

        private void SwapBytesInFileButton_Click( object sender, RoutedEventArgs e )
        {

        }

        private void XORKeyButton_Click( object sender, RoutedEventArgs e )
        {

        }

        private void XORDataButton_Click( object sender, RoutedEventArgs e )
        {

        }



        private void HexTextBox_SelectionChanged( object sender, RoutedEventArgs e )
        {
            if( HexTextBox != null && ASCIITextBox != null )
            {
                ASCIITextBox.Focus();
            }
        }

        private void ASCIITextBox_SelectionChanged( object sender, RoutedEventArgs e )
        {
            if( HexTextBox != null && ASCIITextBox != null )
            {
                HexTextBox.Focus();
            }
        }

        private void HexTextBox_GotFocus( object sender, RoutedEventArgs e )
        {
            if( !HexTextBox.IsMouseOver )
            {
                HexTextBox.CaretIndex = ASCIITextBox.CaretIndex;
                HexTextBox.Select( ASCIITextBox.SelectionStart, ASCIITextBox.SelectionLength );
            }
        }

        private void ASCIITextBox_GotFocus( object sender, RoutedEventArgs e )
        {
            if( !ASCIITextBox.IsMouseOver )
            {
                ASCIITextBox.CaretIndex = HexTextBox.CaretIndex;
                ASCIITextBox.Select( HexTextBox.SelectionStart, HexTextBox.SelectionLength );
            }
        }



        private void HexTextBox_LostFocus( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
        }

        private void ASCIITextBox_LostFocus( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
        }

        private void XORButton_Click( object sender, RoutedEventArgs e )
        {

        }

        private void ColumnFormatUpDownControl_ValueChanged( object sender, RoutedPropertyChangedEventArgs<object> e )
        {

        }

        private void RowFormatUpDownControl_ValueChanged( object sender, RoutedPropertyChangedEventArgs<object> e )
        {

        }

        private void SearchButton_Click( object sender, RoutedEventArgs e )
        {

        }

        private void LogoButton_Click( object sender, RoutedEventArgs e )
        {
            System.Diagnostics.Process.Start( "https://www.sat-4-all.com/board/index.php" );
        }

        private void CopyToClipboardButton_Click( object sender, RoutedEventArgs e )
        {
            Clipboard.SetText( XORResultTextBox.Text, TextDataFormat.Text );
        }


        private string filePathToOpen;
    }
}
