﻿using System;
using System.Text;
using SpecHexEditor_2.Project.Source.Views.ViewHelpers;
using System.IO;


namespace SpecHexEditor_2.Project.Source.Models
{
    public class BinaryFileLogic : BinaryFileModel
    {
        /// <summary>
        /// Opens and reads the binary file under the given path
        /// </summary>
        public void ReadBinaryFile( string pathToFile )
        {
            try
            {
                FileStream binaryFile = File.Open( pathToFile, FileMode.Open, FileAccess.Read );
                while( binaryFile.Position < binaryFile.Length )
                {
                    binaryFileContent.Add( Convert.ToByte( binaryFile.ReadByte() ) );
                }
            }
            catch( FileNotFoundException e )
            {
                System.Windows.MessageBox.Show( e.Message );
            }
        }


        /// <summary>
        /// Writes into the binary file content bytes given as string
        /// </summary>
        public void ParseBinaryContent( string bytes )
        {
            foreach( char byteAsCharacter in bytes )
            {
                binaryFileContent.Add( Convert.ToByte( byteAsCharacter ) );
            }
        }


        /// <summary>
        /// Reads one byte under given index (starting from 0) and converts it to ASCII
        /// </summary>
        public string PrintSingleByteAsHex( int byteNumber )
        {
            if( byteNumber < binaryFileContent.Count )
            {
                iteratorPosition = byteNumber;
                byte[] nextByte = new byte[1];
                nextByte[0] = binaryFileContent[iteratorPosition];
                return Formatter.ByteArrayToHEXString( nextByte, nextByte.Length, nextByte.Length );
            }
            else
            {
                System.Windows.MessageBox.Show( "Byte index exceeded the file size" );
                return "ERROR";
            }
        }


        /// <summary>
        /// Reads next byte as HEX from the file content, basing on the last index read.
        /// </summary>
        public string PrintNextByteAsHex()
        {
            if( iteratorPosition <= binaryFileContent.Count )
            {
                byte[] nextByte = new byte[1];
                nextByte[0] = binaryFileContent[++iteratorPosition];
                return Formatter.ByteArrayToHEXString( nextByte, nextByte.Length, nextByte.Length );
            }
            else
            {
                return "";
            }
        }


        /// <summary>
        /// Reads the whole range of bytes presented as HEX
        /// </summary>
        public string PrintRangeAsHex( int rangeStart, int rangeCount, bool spaceSeparated = false )
        {
            return Formatter.ByteArrayToHEXString(
                binaryFileContent.GetRange( rangeStart, rangeCount ).ToArray(),
                rangeCount,
                rangeCount );
        }


        /// <summary>
        /// Reads the whole binary content and returns it as string of HEX
        /// </summary>
        public string PrintWholeFileAsHex( int bytesInColumn, int bytesInRow )
        {
            return Formatter.ByteArrayToHEXString( binaryFileContent.ToArray(), bytesInColumn, bytesInRow );
        }


        /// <summary>
        /// Reads one byte under given index (starting from 0) and converts it to string of ASCII
        /// </summary>
        public string PrintSingleByteAsASCII( int byteNumber )
        {
            byte[] singleByte = new byte[1];
            singleByte[0] = binaryFileContent[byteNumber];

            iteratorPosition = byteNumber;
            return Encoding.ASCII.GetString( singleByte );
        }


        /// <summary>
        /// Reads next byte as ASCII from the file content, basing on the last index read.
        /// </summary>
        public string PrintNextByteAsASCII()
        {
            if( iteratorPosition <= binaryFileContent.Count )
            {
                byte[] singleByte = new byte[1];
                singleByte[0] = binaryFileContent[++iteratorPosition];
                return Encoding.ASCII.GetString( singleByte );
            }
            else
            {
                return "";
            }
        }


        /// <summary>
        /// Reads the whole range of bytes presented as ASCII
        /// </summary>
        public string PrintRangeAsASCII( int rangeStart, int rangeEnd, bool spaceSeparated = false )
        {
            if( rangeStart > rangeEnd )
            {
                return "ERROR!";
            }

            string bytes = "";
            for( int index = rangeStart; index < rangeEnd; ++index )
            {
                byte[] singleByte = new byte[1];
                singleByte[0] = binaryFileContent[index];
                bytes += Encoding.ASCII.GetString( singleByte ); ;
                if( spaceSeparated )
                {
                    bytes += " ";
                }
            }

            return bytes;
        }


        /// <summary>
        /// Reads the whole binary content and returns it as string of ASCII
        /// </summary>
        public string PrintWholeFileAsASCII( bool spaceSeparated = false )
        {
            string fileContent = "";
            foreach( byte nextByte in binaryFileContent )
            {
                byte[] singleByte = new byte[1];
                singleByte[0] = nextByte;
                fileContent += Encoding.ASCII.GetString( singleByte );
                if( spaceSeparated )
                {
                    fileContent += " ";
                }
            }

            return fileContent;
        }


        /// <summary>
        /// Resets the index position to zero. ReadNextByte will read first byte.
        /// </summary>
        public void ResetPosition()
        {
            iteratorPosition = 0;
        }


        /// <summary>
        /// Sets the new position of the index.
        /// </summary>
        public void SetPosition(int newPosition)
        {
            if( newPosition <= binaryFileContent.Count )
            {
                iteratorPosition = newPosition;
            }
        }
    }
}
