﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecHexEditor_2.Project.Source.Models
{
    public abstract class BinaryPresentationModel
    {
        /// <summary>
        /// HEX representation of the binary file.
        /// Text formating can be modified by helper formatter.
        /// </summary>
        public string binaryFileRepresentationAsHex
        { get; set; }

        /// <summary>
        /// ASCII Representation of the binary file.
        /// </summary>
        public string binaryFileRepresentationAsASCII
        { get; set; }
    }
}
