﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecHexEditor_2.Project.Source.Models;
using SpecHexEditor_2.Project.Source.Views.ViewHelpers;

namespace SpecHexEditor_2.UnitTests.ModelsTests
{
    [TestClass]
    public class XORTests
    {
        [TestMethod]
        public void DoesCalculateXOR()
        {
            byte[] dataForXOR =     { 0b00000000, 0b00000001, 0b00000010, 0b00000011,
                                      0b00000100, 0b00000101, 0b00000110, 0b00000111,
                                      0b00001000, 0b00001001, 0b00001010, 0b00001011 };

            byte[] keyforXOR =      { 0b00001010, 0b00000101, 0b00001001, 0b00000110 };

            byte[] expectedBytes = { (byte)(dataForXOR[0]^keyforXOR[0]), (byte)(dataForXOR[1]^keyforXOR[1]), (byte)(dataForXOR[2]^keyforXOR[2]), (byte)(dataForXOR[3]^keyforXOR[3]),
                                     (byte)(dataForXOR[4]^keyforXOR[0]), (byte)(dataForXOR[5]^keyforXOR[1]), (byte)(dataForXOR[6]^keyforXOR[2]), (byte)(dataForXOR[7]^keyforXOR[3]),
                                     (byte)(dataForXOR[8]^keyforXOR[0]), (byte)(dataForXOR[9]^keyforXOR[1]), (byte)(dataForXOR[10]^keyforXOR[2]), (byte)(dataForXOR[11]^keyforXOR[3]) };

            xor = new XORLogic( keyforXOR, dataForXOR );

            string expected = Formatter.ByteArrayToHEXString( expectedBytes, expectedBytes.Length, expectedBytes.Length );
            xor.CalculateXOR();
            Assert.AreEqual( expected, xor.PrintResult() );
        }


        [TestMethod]
        public void DoesCalculateUnevenXOR()
        {
            byte[] dataForXOR =     { 0b00000000, 0b00000001, 0b00000010, 0b00000011,
                                      0b00000100, 0b00000101, 0b00000110, 0b00000111,
                                      0b00001000, 0b00001001 };

            byte[] keyforXOR =      { 0b00001010, 0b00000101, 0b00001001, 0b00000110 };

            byte[] expectedBytes = { (byte)(dataForXOR[0]^keyforXOR[0]), (byte)(dataForXOR[1]^keyforXOR[1]), (byte)(dataForXOR[2]^keyforXOR[2]), (byte)(dataForXOR[3]^keyforXOR[3]),
                                     (byte)(dataForXOR[4]^keyforXOR[0]), (byte)(dataForXOR[5]^keyforXOR[1]), (byte)(dataForXOR[6]^keyforXOR[2]), (byte)(dataForXOR[7]^keyforXOR[3]),
                                     (byte)(dataForXOR[8]^keyforXOR[0]), (byte)(dataForXOR[9]^keyforXOR[1]) };

            xor = new XORLogic( keyforXOR, dataForXOR );

            string expected = Formatter.ByteArrayToHEXString( expectedBytes, expectedBytes.Length, expectedBytes.Length );
            xor.CalculateXOR();
            Assert.AreEqual( expected, xor.PrintResult() );
        }


        private XORLogic xor;
    }
}
