﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecHexEditor_2.Project.Source.Models
{
    public abstract class XORModel
    {
        /// <summary>
        /// Constructor with parameters as Key for XOR and Data for XOR
        /// </summary>
        public XORModel( byte[] key, byte[] data )
        {
            Key = key;
            Data = data;

            Result = new List<byte>();
        }


        /// <summary>
        /// Key for XOR
        /// </summary>
        protected byte[] Key
        { get; set; }


        /// <summary>
        /// Data for XOR
        /// </summary>
        protected byte[] Data
        { get; set; }


        /// <summary>
        /// Result of XOR calculation
        /// </summary>
        protected List<byte> Result
        { get; set; }
    }
}
